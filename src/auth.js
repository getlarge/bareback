const auth = require('basic-auth');
const { cred } = require('./config');

const user = cred.user;
const pass = cred.pass;
const admins = { user: { password: pass } };

module.exports = function (request, response, next) {
  var user = auth(request);
  if (!user || !admins[user.name] || admins[user.name].password !== user.pass) {
    response.set('WWW-Authenticate', 'Basic realm="example"');
    return response.status(401).send();
  }
  return next();
};