const del = require("del");

const imageFilter = function(req, file, cb) {
    if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
        return cb(new Error("Only image files are allowed!"), false);
    }
    cb(null, true);
};

const audioFilter = function(req, file, cb) {
    if (!file.originalname.match(/\.(mp3|webm|mpeg|wav)$/)) {
        return cb(new Error("Only audio files are allowed!"), false);
    }
    cb(null, true);
};

const textFilter = function(req, file, cb) {
    if (!file.originalname.match(/\.(txt|md|html|json)$/)) {
        return cb(new Error("Only text files are allowed!"), false);
    }
    cb(null, true);
};

const cleanFolder = function(folderPath) {
    // delete files inside folder but not the folder itself
    del.sync([`${folderPath}/**`, `!${folderPath}`]);
};

module.exports = {
    audioFilter,
    imageFilter,
    textFilter,
    cleanFolder
};
