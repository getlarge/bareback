const path = require("path");
const http = require("http");
const https = require("https");
const express = require("express");
const serveStatic = require("serve-static");
const bodyParser = require("body-parser");
const cors = require("cors");
const multer = require("multer");

const auth = require("./auth");
const { httpConf, httpsConf } = require("./config");
const secureServer = process.env.SERVER_SECURE || "false";
const { audioFilter, imageFilter, textFilter, cleanFolder } = require("./utils");
const {
    loadCollection,
    uploadMediaItem,
    uploadJournalItem,
    loadCollectionByName,
    loadPostByParam,
    loadItemByParam,
    loadItemById
} = require("./db");

const IMAGE_COLLECTION = "images";
const AUDIO_COLLECTION = "audios";
const TEXT_COLLECTION = "texts";
const UPLOAD_PATH = process.env.UPLOAD_PATH || "uploads";
const uploadImage = multer({ dest: `${UPLOAD_PATH}/`, fileFilter: imageFilter });
const uploadAudio = multer({ dest: `${UPLOAD_PATH}/`, fileFilter: audioFilter });
const uploadText = multer({ dest: `${UPLOAD_PATH}/`, fileFilter: textFilter });

const bareback = express();

const options = {
    dotfiles: "ignore",
    etag: false,
    extensions: ["htm", "html"],
    index: false,
    maxAge: "1d",
    redirect: false,
    setHeaders: setCustomCacheControl
};

function setCustomCacheControl(res, path) {
    if (serveStatic.mime.lookup(path) === "text/html") {
        res.setHeader("Cache-Control", "public, max-age=0");
    } else {
        res.setHeader("Content-Disposition", contentDisposition(path));
    }
}

//bareback.use(auth);

// Delete loki database
//cleanFolder(UPLOAD_PATH);

/// todo : bloquer les requêtes ne provenant pas du domaine .getlarge
bareback.use(cors());

bareback.use(serveStatic(path.join(__dirname, "../public")));
//bareback.use(serveStatic(path.join(__dirname, 'public'), staticOptions));
bareback.use(bodyParser.json());
bareback.use(
    bodyParser.urlencoded({
        extended: true
    })
);

const httpServer = http.createServer(bareback);
httpServer.listen(httpConf.port, () => {
    console.log("HTTP Server running on port " + httpConf.port);
});

if (secureServer !== "false") {
    const httpsServer = https.createServer(httpsConf.credentials, bareback);
    httpsServer.listen(httpsConf.port, () => {
        console.log("HTTPS Server running on port " + httpsConf.port);
    });
}

bareback.get("/", async (req, res) => {
    console.log("req", req.url);
    try {
        res.send([
            {
                title: "Hi, I'm the bareback server!",
                description: "Start broka-billy and go frontin !"
            }
        ]);
    } catch (err) {
        res.sendStatus(400);
    }
});

/// IMAGE RESSOURCES
bareback
    .route("/images")
    .post(uploadImage.array("images", 12), async (req, res) => {
        console.log("images upload");
        uploadMediaItem(req, res, IMAGE_COLLECTION);
    })
    .get(async (req, res) => {
        console.log("image collection");
        loadCollectionByName(req, res, IMAGE_COLLECTION);
    });

bareback.get("/images/:id", async (req, res) => {
    console.log("image by id");
    loadItemById(req, res, IMAGE_COLLECTION);
});

bareback.get("/images/name/:originalname", async (req, res) => {
    console.log("image by name");
    loadItemByParam(req, res, IMAGE_COLLECTION);
});

/// AUDIO RESSOURCES
bareback
    .route("/audios")
    .post(uploadAudio.array("audios", 12), async (req, res) => {
        console.log("audios upload");
        uploadMediaItem(req, res, AUDIO_COLLECTION);
    })
    .get(async (req, res) => {
        console.log("audio collection");
        loadCollectionByName(req, res, AUDIO_COLLECTION);
    });

bareback.get("/audios/:id", async (req, res) => {
    console.log("audio by id");
    loadItemById(req, res, AUDIO_COLLECTION);
});

bareback.get("/audios/name/:originalname", async (req, res) => {
    console.log("audio by name");
    loadItemByParam(req, res, AUDIO_COLLECTION);
});

/// JOURNAL COMPONENT RESSOURCES
bareback
    .route("/texts")
    .post(uploadText.single("text"), async (req, res) => {
        console.log("texts upload");
        uploadJournalItem(req, res, TEXT_COLLECTION);
    })
    .get(async (req, res) => {
        console.log("text collection");
        loadCollectionByName(req, res, TEXT_COLLECTION);
    });

bareback.get("/texts/:id", async (req, res) => {
    console.log("text by id", req.params.id);
    loadItemById(req, res, TEXT_COLLECTION);
});

bareback.get("/texts/name/:originalname", async (req, res) => {
    console.log("text by name");
    loadItemByParam(req, res, TEXT_COLLECTION);
});

bareback.get("/texts/page/:page", async (req, res) => {
    console.log("text by page");
    loadPostByParam(req, res, TEXT_COLLECTION);
});
