const Loki = require("lokijs");
const fs = require("fs");
const path = require("path");
const DB_NAME = process.env.DB_NAME || "db.json";
const UPLOAD_PATH = process.env.UPLOAD_PATH || "uploads";
const db = new Loki(`${UPLOAD_PATH}/${DB_NAME}`, { persistenceMethod: "fs" });

// loadCollection: load a collection from db
//   params:
//       colName <string>	: collection name to load
//       db <object>		: database instance
//   returns:
//       <object> : return collection
const loadCollection = function(colName, db) {
	return new Promise(resolve => {
		db.loadDatabase({}, () => {
			const _collection = db.getCollection(colName) || db.addCollection(colName);
			resolve(_collection);
		});
	});
};

// uploadMediaItem: upload a media ( audio or image ) into the db
//   params:
//       req <object>     : express received request
//       res <object>   : express response
//       collection <string> : collection in which upload will happen
//   returns:
//       <boolean> : telling if operation succeeded
const uploadMediaItem = async function(req, res, collection) {
	try {
		const col = await loadCollection(collection, db);
		let data = [].concat(col.insert(req.files));
		db.saveDatabase();
		res.send(
			data.map(x => ({ id: x.$loki, fileName: x.filename, originalName: x.originalname }))
		);
	} catch (err) {
		res.sendStatus(400);
		return false;
	}
	return true;

};

const uploadJournalItem = async function(req, res, collection) {
	try {
		const col = await loadCollection(collection, db);
		let data = [].concat(req.file);
		//console.log(req.body);
		data = data.map(file =>
			Object.assign({}, file, {
				journalId: `${Number(req.body.journalId)}`,
				page: `${Number(req.body.page)}`
			})
		);
		col.insert(data);
		console.log(data);
		db.saveDatabase();
		res.send(
			data.map(x => ({
				id: x.$loki,
				fileName: x.filename,
				originalName: x.originalname,
				journalId: x.journalId,
				page: x.page
			}))
		);
	} catch (err) {
		res.sendStatus(400);
	}
};

const loadCollectionByName = async function(req, res, collection) {
	try {
		const col = await loadCollection(collection, db);
		res.send(col.data);
	} catch (err) {
		res.sendStatus(400);
	}
};

const loadPostByParam = async function(req, res, collection) {
	try {
		const col = await loadCollection(collection, db);
		console.log(req.params);
		const result = col.find(req.params);
		if (!result) {
			res.sendStatus(404);
			return;
		}
		console.log("where filter: ");
		console.log(result);
		const i = result.length;
		const title = result[i - 1].originalname.split(".");
		fs.readFile(path.join(UPLOAD_PATH, result[i - 1].filename), "utf8", function(err, data) {
			if (err) throw err;
			//console.log(data.toString());
			res.json({ name: title[0], text: data.toString() });
		});
	} catch (err) {
		res.sendStatus(400);
		console.log("error", err);
	}
};

const loadItemByParam = async function(req, res, collection) {
	try {
		const col = await loadCollection(collection, db);
		console.log(req.params);
		const result = col.find(req.params);
		if (!result) {
			res.sendStatus(404);
			return;
		}
		console.log("where filter: ");
		console.log(result);
		res.setHeader("Content-Type", result[0].mimetype);
		var stream = fs
			.createReadStream(path.join(UPLOAD_PATH, result[0].filename), { autoClose: true })
			.on("open", function() {
				stream.pipe(res);
			})
			.on("end", function() {
				console.log("stream end");
			})
			.on("error", function(err) {
				res.end(err);
			});
		//fs.createReadStream(path.join(UPLOAD_PATH, result[0].filename)).pipe(res);
	} catch (err) {
		res.sendStatus(400);
		console.log("error", err);
	}
};

const loadItemById = async function(req, res, collection) {
	try {
		const col = await loadCollection(collection, db);
		const result = col.get(req.params.id);
		if (!result) {
			res.sendStatus(404);
			return;
		}
		res.setHeader("Content-Type", result.mimetype);
		fs.createReadStream(path.join(UPLOAD_PATH, result.filename)).pipe(res);
	} catch (err) {
		res.sendStatus(400);
	}
};

module.exports = {
	loadCollection,
	uploadMediaItem,
	uploadJournalItem,
	loadCollectionByName,
	loadPostByParam,
	loadItemByParam,
	loadItemById
};
