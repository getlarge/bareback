const fs = require("fs");

const user = process.env.SERVER_USER || "";
const pass = process.env.SERVER_PASS || "";
const cred = { user: user, pass: pass };
const serverPort = Number(process.env.SERVER_PORT) || 3000;
const httpConf = {
	port: serverPort
};
const serverPortSecure = Number(process.env.SERVER_PORT_SECURE) || 4000;
const privateKeyPath = process.env.PRVKEY_PATH || "";
const certificatePath = process.env.FULLCHAIN_PATH || "";
const caPath = process.env.CA_PATH || "";
const privateKey = privateKeyPath !== "" ? fs.readFileSync(privateKeyPath, "utf8") : "";
const certificate = certificatePath !== "" ? fs.readFileSync(certificatePath, "utf8") : "";
const ca = certificatePath !== "" ? fs.readFileSync(caPath, "utf8") : "";
const httpsConf = {
	port: serverPortSecure,
	credentials: {
		key: privateKey,
		cert: certificate,
		ca: ca
	}
};

module.exports = { cred, httpConf, httpsConf };
